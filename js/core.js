var map;
var table;
var markers = [];
var api_endpoint = 'XBUS_API_ENDPOINT';

function initialize() {
    var mapOptions = {
        zoom: 5,
        center: new google.maps.LatLng(35.8574708,104.1361118),
        //disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        },
        panControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        }
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
}

var deselect_btn = function(obj) {
    if (is_btn_clicked(obj))
        $(obj).click();
}

var set_btn_tag_name = function(obj, text) {
    $(obj).find('span.tag-name').text(text);
    $(obj).find('span.tag-name').attr('data-tag-name', text);
}

var get_btn_tag_name = function(obj) {
    return $(obj).find('span.tag-name').attr('data-tag-name');
}

//close search result box
var clear_search_result = function() {
    var btn = '.current-search-tag a.btn';
    deselect_btn(btn);

    $('.search-term').text('');
    $(btn + ' .badge').text('0');

    $('.current-search-tag').addClass('hide');
};

var is_tag_exist = function(search_from, tag) {
    return $(search_from).find('[data-tag-name="'+tag+'"]').length > 0;
}

var is_btn_clicked = function(obj) {
    return $(obj).hasClass('btn-info');
}

//click on tag
$('html').on('click', '.list-box a.btn', function() {
    if (is_btn_clicked(this)) {
        text = get_btn_tag_name(this);
        if (markers[text] != undefined) {
            for (var x in markers[text]) {
                markers[text][x].setMap(null);
            }
            markers[text].length = 0;
        }
    } else {
        search_tag(this);
    }
    $(this).toggleClass('btn-default').toggleClass('btn-info');
    return false;
});

$('html').on('click', '.current-search-tag .close', function() {
    $(".search-query").val('');
    clear_search_result();
    return false;
});

//save tag
$('html').on('click', '.list-box .save-tag-btn', function() {
    var term = get_btn_tag_name($(this).parent());
    packed_json = {};
    packed_json['tag_name'] = term;
    packed_json['poi_counts'] = table.api().data().length;
    packed_json['poi'] = {};
    $.each(table.api().data(), function(k, v) {
        packed_json['poi'][k] = v;
        packed_json['poi'][k]['icon_type'] = 0;
    });
    $.ajax({url: api_endpoint + "/api_save_search_result", 
        data: JSON.stringify(packed_json), 
        dataType: "json", processData: false, type: "post", 
        statusCode: {
            200: function(data) {
                get_tags();
            },
            500: function() {
                alert("BOOM");
            }
        }
    });
    return false;
});

//delete tag
$('html').on('click', '.list-box .tag .close', function() {
    if (! confirm('移除此標籤？'))
        return false;

    deselect_btn($(this).parent());

    var term = get_btn_tag_name($(this).parent());
    $.ajax({url: api_endpoint + "/api_delete_tag", data: "tag_name="+term, dataType: "json", type: "get", 
        statusCode: {
            200: function(data) {
                $('.list-tag span.tag-name[data-tag-name="'+term+'"').parents('.btn').remove();
            },
            500: function() {
                alert("BOOM");
            }
        }
    });
    return false;
});

$('html').on('click', '.find-on-map', function() {
    var latlng = $(this).attr('title');
    for (n in markers)
      for (m in markers[n])
        if (markers[n][m].latlng == latlng) {
            google.maps.event.trigger(markers[n][m], 'click');
            map.panTo(markers[n][m].getPosition());
        }
    return false;
});

//get_tags
var get_tags = function() {
    $.ajax({url: api_endpoint + "/api_get_tags", dataType: "json", type: "get",
        statusCode: {
            200: function(data) {
                $.each(data, function(k, v) {
                    if (! is_tag_exist('.list-tag', v.tag_name)) {
                        tag = '<a class="tag btn btn-default">'
                              + '<span class="tag-name" data-tag-name="'+v.tag_name+'">'+v.tag_name+'</span> <span class="badge">'+v.poi_counts
                              +'</span><span class="close">×</span></a>'+"\n";
                        $('.list-tag').append(tag);
                    }
                });
            },
            500: function() {
                alert("BOOM");
            }
        }
    });
}

//submit search form
$('html').on('submit', '#search-form', function() {
    clear_search_result();

    var btn = '.current-search-tag a.btn';
    deselect_btn(btn);
    set_btn_tag_name(btn, $(".search-query").val());
    $(btn).click();

    $('.current-search-tag').removeClass('hide');
    return false;
});

//search tag
var search_tag = function(obj) {
    var term = get_btn_tag_name(obj);
    $('.search-term').text(' ('+term+')');

    table.api().ajax.url( api_endpoint + "/api_search_tag?search_tag=" + term).load();
}

$(function() {
    get_tags();
    initialize();
    table = $('#table').dataTable({
        /*"columnDefs": [{
            "targets": [0],
            "bSortable": false
        }],*/
        "order": [[ 2, "asc" ]],
        "paging": false,
        "searching": false,
        "info": false,
        "ajax": {
            "url": "test.json",
            "dataSrc": "poi"
        },
        "columns": [
            { "data": "stop_name", "className": "" },
            { "data": "route_name" },
            { "data": "region" },
            //{ "data": "icon_type" },
            { "data": "latlng" }
        ],
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var text = "";
            if (aData.latlng != "x,y") {
                text = "<a class=\"btn btn-link find-on-map\" title=\""+aData.latlng+"\">定位</a>"
            }
            $('td:eq(3)', nRow).html(text);
        }
    });

    table.api().on('xhr', function ( e, settings, json ) {
        if (json.tag_name == get_btn_tag_name('.current-search-tag a.btn'))
            $('.current-search-tag').find('span.badge').text(json.poi.length);
                
        if (is_tag_exist('.btn-info', json.tag_name)) {
            if (markers[json.tag_name] == undefined)
                markers[json.tag_name] = [];

            $.each(json.poi, function(a, aData) {
                if (aData.latlng != 'x,y') {
                    var infowindow = new google.maps.InfoWindow({
                        content: '<div class="info-window"><strong>' + aData.stop_name + '</strong> ('
                                  + aData.region + ')<br>' + aData.route_name + '<br>'
                                  + aData.latlng + '</div>'
                    });

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(parseFloat(aData.latlng.split(',')[1])-0.0065, parseFloat(aData.latlng.split(',')[0])-0.0065),
                        map: map,
                        title: aData.stop_name + ' (' + aData.region + ')',
                        latlng: aData.latlng
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        for (var n in markers) {
                            for (var m in markers[n]) {
                                markers[n][m].infowindow.close();
                            }
                        }
                        infowindow.open(map, marker);
                    });
                    marker.infowindow = infowindow;
                    markers[json.tag_name].push(marker);
                }
            });
            return;
        }

        $('.sidebar-inside').perfectScrollbar('destroy');
        $('.sidebar-inside').perfectScrollbar({suppressScrollX: true});
    });

    $('.sidebar-inside').perfectScrollbar({suppressScrollX: true});
});
